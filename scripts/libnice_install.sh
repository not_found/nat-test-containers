# Install libnice package and dependencies

echo
echo "Install packages in machine"

apt-get install -y stuntman-server


echo
echo "Install packages for containers"

for c in peer0 peer1
do

    echo "Install packages for $c"

    lxc exec $c -- apk add build-base
    lxc exec $c -- apk add py-pip
    lxc exec $c -- pip install pip --upgrade
    lxc exec $c -- pip install conan

    lxc exec $c -- apk add glib  #still required for runtime dependency
    lxc exec $c -- apk add gettext-dev

    lxc exec $c -- conan install libnice/0.1.13@noface/experimental --build missing

done
