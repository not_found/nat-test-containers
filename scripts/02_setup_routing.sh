echo
echo "Setup routes"

for p in peer0 peer1
do
    echo
    echo "$p -> router"

    echo push "${p}_routing" "$p/etc/network/if-up.d/"
    lxc file push "${p}_routing" "$p/etc/network/if-up.d/"
    lxc exec $p -- /etc/init.d/networking restart # restart network
    lxc exec $p -- ip route show
done

for r in router0 router1
do
    echo
    echo "$r -> host (default)"
    lxc exec $r -- ip route del default
    lxc exec $r -- ip route add default via 10.1.1.1
    lxc exec $r -- ip route show
done

echo
echo "Setup packet forwarding"

echo
echo "Install iptables on routers"

for r in router0 router1
do
    lxc exec $r -- apk add iptables
    lxc exec $r -- iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
done

echo
echo "Configure routers package forward"

echo "Configuring iptables routes"

lxc exec router0 -- iptables -A FORWARD -d 192.168.1.0/24 -j ACCEPT  -m comment --comment "Accept forward packages to local network"

lxc exec router1 -- iptables -A FORWARD -d 192.168.2.0/24 -j ACCEPT  -m comment --comment "Accept forward packages to local network"


for r in router0 router1
do
    echo "Configure rules for router $r"

    lxc exec $r -- iptables -A FORWARD -d 192.168.0.0/16 -j REJECT --reject-with icmp-net-unreachable \
	    -m comment --comment "Reject routing for other private networks"

    # Accept packets from a already stablished connection (may not be needed)
    lxc exec $r -- iptables -A FORWARD -i eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT

    # DROP new packets from public interface, to avoid default behaviour 
    # (ICMP unreachable port) that prevents NAT traversal
    lxc exec $r -- iptables -A INPUT -i eth1 -m state --state NEW -j DROP

    echo
    echo "Saving iptables state"
    lxc exec $r -- rc-update add iptables  #Set iptables to start on reboot 
    lxc exec $r -- /etc/init.d/iptables save #Write iptable rules to disk

done
