echo "Criando hosts"
#lxc launch images:alpine/3.4 peer0
#lxc launch images:alpine/3.4 peer1
#lxc launch images:alpine/3.4 router0
#lxc launch images:alpine/3.4 router1

for c in peer0 peer1 router0 router1
do
    lxc init images:alpine/3.4 $c
done

lxc list

echo
echo "Creating networks"
lxc network create priv0 ipv4.address=192.168.1.1/24 ipv4.nat=false ipv4.firewall=false ipv6.address=none
lxc network create priv1 ipv4.address=192.168.2.1/24 ipv4.nat=false ipv4.firewall=false ipv6.address=none
lxc network create public0 ipv4.address=10.1.1.1/24 ipv4.nat=true ipv6.address=none
lxc network list

echo
echo "Conecting hosts to networks"
lxc network attach priv0 peer0
lxc network attach priv1 peer1
lxc network attach priv0 router0
lxc network attach priv1 router1
lxc network attach public0 router0
lxc network attach public0 router1
lxc network list

echo
echo "Setup routers interfaces"

echo "Configure routers interfaces"
lxc file push interfaces_config.txt router0/etc/network/interfaces
lxc file push interfaces_config.txt router1/etc/network/interfaces

lxc config device set router0 priv0 ipv4.address 192.168.1.100
lxc config device set router0 public0 ipv4.address 10.1.1.100

lxc config device set router1 priv1 ipv4.address 192.168.2.200
lxc config device set router1 public0 ipv4.address 10.1.1.200


echo
echo "starting containers"

for c in peer0 peer1 router0 router1
do
    lxc start $c
done

#echo
#echo "restart routers interfaces"
#lxc exec router0 -- ifdown -a
#lxc exec router0 -- ifup -a

#lxc exec router1 -- ifdown -a
#lxc exec router1 -- ifup -a

lxc list

# Expected network:
# +---------+---------+--------------------------------+------+------------+-----------+
# |  NAME   |  STATE  |              IPV4              | IPV6 |    TYPE    | SNAPSHOTS |
# +---------+---------+--------------------------------+------+------------+-----------+
# | peer0   | RUNNING | 192.168.1.? (eth0)             |      | PERSISTENT | 0         |
# +---------+---------+--------------------------------+------+------------+-----------+
# | peer1   | RUNNING | 192.168.2.? (eth0)             |      | PERSISTENT | 0         |
# +---------+---------+--------------------------------+------+------------+-----------+
# | router0 | RUNNING | 192.168.1.100 (eth0)           |      | PERSISTENT | 0         |
# |         |         | 10.1.1.100 (eth1)              |      |            |           |
# +---------+---------+--------------------------------+------+------------+-----------+
# | router1 | RUNNING | 192.168.2.200 (eth0)           |      | PERSISTENT | 0         |
# |         |         | 10.1.1.200 (eth1)              |      |            |           |
# +---------+---------+--------------------------------+------+------------+-----------+

