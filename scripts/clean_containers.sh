for container in peer0 peer1 router0 router1
do
    echo "stop container" $container
    lxc stop $container
    echo "delete container " $container
    lxc delete $container
done

for net in public0 priv0 priv1
do
    echo "removing network $net"
    lxc network delete $net
done
