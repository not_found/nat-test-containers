echo "Upgrading lxd from ppa"
add-apt-repository ppa:ubuntu-lxc/lxd-stable
apt-get update -y
#apt-get dist-upgrade -y
apt-get install -y lxd

echo "lxd version:"
lxd --version

echo
echo "Initing lxd"
lxd init --auto
